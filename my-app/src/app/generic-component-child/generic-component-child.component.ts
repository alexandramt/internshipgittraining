import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-generic-component-child',
  templateUrl: './generic-component-child.component.html',
  styleUrls: ['./generic-component-child.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GenericComponentChildComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
